/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import minesweepermvc.MinesweeperModel;
import minesweepermvc.ScoreModel;

/**
 *
 * @author liush
 
public class MinesweeperTest {
    
    public MinesweeperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
*/
public class MinesweeperTest {

	@BeforeClass
	public static void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void MinesweeperTest() throws Exception {
	}

	@Test
	/**
	 * Test constructor and display contents in console
	 */
	public void testMinesweeperModelConstructor() {
		MinesweeperModel mTest = new MinesweeperModel();
		System.out.println(mTest);
		assertNotNull(mTest);
	}
	
	@Test
	public void testScoreModel() {
		ScoreModel sTest = new ScoreModel();
		sTest.clearScores();
		sTest.saveScores();
		
		assertNotNull(sTest);
		assertEquals(0, sTest.size());
		
		sTest.addScore("Alicia", 192);
		sTest.addScore("Jimmy", 211);
		sTest.addScore("Jane", 412);
		sTest.addScore("Billy", 172);
		assertEquals(4, sTest.size());
		
		sTest.addScore("Tom", 512);
		sTest.addScore("Julie", 210);
		assertEquals(6, sTest.size());
		
		sTest.saveScores();
		System.out.println(sTest +"\n");
		
		sTest = new ScoreModel();
		assertEquals(6, sTest.size());
		assertTrue(sTest.isTopScore(50000));
		
		sTest.addScore("Sam", 513);
		sTest.addScore("Pam", 512);
		sTest.addScore("Caleb", 522);
		sTest.addScore("Kayla", 128);
		System.out.println(sTest +"\n");
		assertFalse(sTest.isTopScore(5000));
		assertTrue(sTest.isTopScore(172));
		sTest.addScore("Billy", 172);
		assertEquals(10, sTest.size());
		sTest.addScore("Fail", 50000);
		assertEquals(10, sTest.size());
		
		System.out.println(sTest);
	}

}